use aoc2019::read_comma_seperated_ints_from_line;

struct Computer<'a> {
    input: &'a Vec<i32>,
    noun: i32,
    verb: i32,
}

impl<'a> Computer<'a> {
    fn process_intcode(&mut self) -> i32 {
        let mut code = self.input.to_vec();
        code[1] = self.noun;
        code[2] = self.verb;
        for i in (0..code.len() - 1).step_by(4) {
            let instruction = code[i];
            let result = match instruction {
                1 => code[code[i + 1] as usize] + code[code[i + 2] as usize],
                2 => code[code[i + 1] as usize] * code[code[i + 2] as usize],
                99 => return code[0],
                _ => 0,
            };
            let index = code[i + 3] as usize;
            code[index] = result;
        }
        code[0]
    }
}

fn brute_force(input: &Vec<i32>) -> i32 {
    let mut computer = Computer {
        input: &input,
        noun: 0,
        verb: 0,
    };
    'noun: for noun in 0..100 {
        'verb: for verb in 0..100 {
            computer.noun = noun;
            computer.verb = verb;
            let result = computer.process_intcode();
            if result == 19690720 {
                //break 'noun;
                return (100 * noun) + verb;
            }
        }
    }
    0
}

fn main() {
    let input = read_comma_seperated_ints_from_line("day2.in");
    let mut part_one_computer = Computer {
        input: &input,
        noun: 12,
        verb: 2,
    };
    let part_1_answer = part_one_computer.process_intcode();
    assert_eq!(part_1_answer, 2890696);
    println!("Part 1: {}", part_1_answer);
    let part_2_answer = brute_force(&input);
    assert_eq!(part_2_answer, 8226);
    println!("Part 2: {}", part_2_answer);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn process_example_intcode_without_an_op_that_affects_position_0() {
        let mut example_computer = Computer {
            input: &vec![1, 0, 0, 3, 99],
            noun: 0,
            verb: 0,
        };
        assert_eq!(example_computer.process_intcode(), 1);
    }

    #[test]
    fn process_example_intcode_with_just_an_add() {
        let mut example_computer = Computer {
            input: &vec![1, 0, 0, 0, 99],
            noun: 0,
            verb: 0,
        };
        assert_eq!(example_computer.process_intcode(), 2);
    }

    #[test]
    fn process_example_intcode_with_just_a_multiply() {
        let mut example_computer = Computer {
            input: &vec![2, 1, 0, 0, 99],
            noun: 1,
            verb: 0,
        };
        assert_eq!(example_computer.process_intcode(), 2);
    }

    #[test]
    fn process_example_with_two_adds() {
        let mut example_computer = Computer {
            input: &vec![1, 0, 0, 3, 1, 4, 4, 0, 99],
            noun: 0,
            verb: 0,
        };
        assert_eq!(example_computer.process_intcode(), 2);
    }

    #[test]
    fn process_examples() {
        let mut example_computer = Computer {
            input: &vec![1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50],
            noun: 9,
            verb: 10,
        };
        assert_eq!(example_computer.process_intcode(), 3500);
        let mut example_computer = Computer {
            input: &vec![1, 1, 1, 4, 99, 5, 6, 0, 99],
            noun: 1,
            verb: 1,
        };
        assert_eq!(example_computer.process_intcode(), 30);
    }
}
