use aoc2019::read_comma_seperated_strings_from_lines;
use std::collections::HashSet;
use std::ops::Add;

fn main() {
    let instructions = read_comma_seperated_strings_from_lines("day3.in");
    let mut paths: Vec<Vec<Coord>> = instructions
        .iter()
        .map(|instruction_set| path(instruction_set.to_vec()))
        .collect();
    let path_1 = paths.pop().unwrap();
    let path_2 = paths.pop().unwrap();

    let earliest_intersection_distance = find_earliset_intersection_distance(&path_1, &path_2);
    assert_eq!(earliest_intersection_distance, 293);
    println!("Part 1: {:?}", earliest_intersection_distance);

    let earliest_intersection_steps = find_earliest_intersection_steps(path_1, path_2);
    assert_eq!(earliest_intersection_steps, 27306);
    println!("Part 2: {:?}", earliest_intersection_steps);
}

#[derive(Debug, Clone, Copy, Eq, Ord, PartialEq, PartialOrd, Hash)]
struct Coord {
    x: i32,
    y: i32,
}

impl Add for Coord {
    type Output = Coord;

    fn add(self, other: Coord) -> Coord {
        Coord {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

fn path(directions: Vec<String>) -> Vec<Coord> {
    let mut coordinates: Vec<Coord> = Vec::new();
    let mut previous_coord = Coord { x: 0, y: 0 };
    for d in directions.iter() {
        let new_dir = match d.chars().next().unwrap() {
            'L' => Coord {
                x: d.replace('L', "-").parse().unwrap(),
                y: 0,
            },
            'R' => Coord {
                x: d.replace('R', "").parse().unwrap(),
                y: 0,
            },
            'U' => Coord {
                x: 0,
                y: d.replace('U', "").parse().unwrap(),
            },
            'D' => Coord {
                x: 0,
                y: d.replace('D', "-").parse().unwrap(),
            },
            _ => Coord { x: 0, y: 0 },
        };
        let mut x = new_dir.x;
        let mut y = new_dir.y;
        while x != 0 {
            let one_step = x / x.abs();
            coordinates.push(previous_coord + Coord { x: one_step, y: 0 });
            previous_coord = *coordinates.last().unwrap();
            x = x - one_step;
        }
        while y != 0 {
            let one_step = y / y.abs();
            coordinates.push(previous_coord + Coord { x: 0, y: one_step });
            previous_coord = *coordinates.last().unwrap();
            y = y - one_step;
        }
    }
    coordinates
}

fn find_intersections(path_1: &Vec<Coord>, path_2: &Vec<Coord>) -> Vec<Coord> {
    let a: HashSet<_> = path_1.iter().cloned().collect();
    let b: HashSet<_> = path_2.iter().cloned().collect();
    a.intersection(&b).map(|i| *i).collect()
}

fn find_coordinate_nearest_centre(mut coords: Vec<Coord>) -> Coord {
    coords.sort_by(|a, b| (a.x.abs() + a.y.abs()).cmp(&(b.x.abs() + b.y.abs())));
    coords[0]
}

fn find_earliset_intersection_distance(path_1: &Vec<Coord>, path_2: &Vec<Coord>) -> i32 {
    let intersections = find_intersections(&path_1, &path_2);
    let nearest_intersection = find_coordinate_nearest_centre(intersections);
    nearest_intersection.x.abs() + nearest_intersection.y.abs()
}

fn find_earliest_intersection_steps(path_1: Vec<Coord>, path_2: Vec<Coord>) -> usize {
    let intersections = find_intersections(&path_1, &path_2);
    let mut steps: Vec<usize> = Vec::new();
    for intersection in intersections {
        let steps_1 = path_1.iter().position(|&x| x == intersection).unwrap();
        let steps_2 = path_2.iter().position(|&x| x == intersection).unwrap();
        steps.push(steps_1 + steps_2 + 2);
    }
    *steps.iter().min().unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn path_from_single_right_direction_moves_in_positive_x() {
        assert_eq!(
            path(vec![String::from("R3")]),
            vec![
                Coord { x: 1, y: 0 },
                Coord { x: 2, y: 0 },
                Coord { x: 3, y: 0 }
            ]
        );
    }

    #[test]
    fn path_from_single_right_direction_moves_in_negative_x() {
        assert_eq!(
            path(vec![String::from("L3")]),
            vec![
                Coord { x: -1, y: 0 },
                Coord { x: -2, y: 0 },
                Coord { x: -3, y: 0 }
            ]
        );
    }

    #[test]
    fn path_from_single_up_direction_moves_in_positive_y() {
        assert_eq!(
            path(vec![String::from("U3")]),
            vec![
                Coord { x: 0, y: 1 },
                Coord { x: 0, y: 2 },
                Coord { x: 0, y: 3 }
            ]
        );
    }

    #[test]
    fn path_from_single_down_direction_moves_in_negative_y() {
        assert_eq!(
            path(vec![String::from("D3")]),
            vec![
                Coord { x: 0, y: -1 },
                Coord { x: 0, y: -2 },
                Coord { x: 0, y: -3 }
            ]
        );
    }

    #[test]
    fn path_from_multiple_directions_are_cumulative() {
        assert_eq!(
            path(vec![String::from("D3"), String::from("D3")]),
            vec![
                Coord { x: 0, y: -1 },
                Coord { x: 0, y: -2 },
                Coord { x: 0, y: -3 },
                Coord { x: 0, y: -4 },
                Coord { x: 0, y: -5 },
                Coord { x: 0, y: -6 }
            ]
        );
    }

    #[test]
    fn compare_paths_that_intersect_at_all_coords() {
        let mut result = find_intersections(
            &vec![Coord { x: 0, y: -3 }, Coord { x: 0, y: -6 }],
            &vec![Coord { x: 0, y: -3 }, Coord { x: 0, y: -6 }],
        );
        result.sort();
        assert_eq!(result, vec![Coord { x: 0, y: -6 }, Coord { x: 0, y: -3 }]);
    }

    #[test]
    fn find_nearest_coord() {
        assert_eq!(
            find_coordinate_nearest_centre(vec![
                Coord { x: 4, y: 3 },
                Coord { x: 3, y: -3 },
                Coord { x: 1, y: -6 }
            ]),
            Coord { x: 3, y: -3 }
        );
    }

    #[test]
    fn part_2_example() {
        let instruction_string_1: Vec<String> =
            "R8,U5,L5,D3".split(',').map(|i| i.to_string()).collect();
        let instruction_string_2: Vec<String> =
            "U7,R6,D4,L4".split(',').map(|i| i.to_string()).collect();
        let path_1 = path(instruction_string_1);
        let path_2 = path(instruction_string_2);
        assert_eq!(find_earliest_intersection_steps(path_1, path_2), 30);
    }
}
