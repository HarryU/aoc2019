use std::fs::File;
use std::io::{BufRead, BufReader, Error, ErrorKind};

pub fn read_ints_from_file(filename: &str) -> Vec<i32> {
    let reader = create_reader(filename);
    reader
        .lines()
        .map(|line| {
            line.and_then(|value| {
                value
                    .parse()
                    .map_err(|error| Error::new(ErrorKind::InvalidData, error))
            })
        })
        .collect::<Result<Vec<i32>, Error>>()
        .unwrap()
}

pub fn read_comma_seperated_ints_from_line(filename: &str) -> Vec<i32> {
    let reader = create_reader(filename);
    reader
        .lines()
        .next()
        .unwrap()
        .unwrap()
        .split(",")
        .map(|string| string.parse::<i32>().unwrap())
        .collect()
}

pub fn read_comma_seperated_strings_from_lines(filename: &str) -> Vec<Vec<String>> {
    let reader = create_reader(filename);
    reader
        .lines()
        .map(|line| {
            line.unwrap()
                .split(",")
                .map(|instruction| instruction.to_string())
                .collect()
        })
        .collect()
}

fn create_reader(filename: &str) -> BufReader<File> {
    BufReader::new(File::open(filename).unwrap())
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
