import pytest


def fuel_calc(mass):
    return int(int(mass) / 3) - 2


def calculate_fuels(masses):
    total = []
    for mass in masses:
        fuel = fuel_calc(mass)
        total.append(fuel)
    return total


def calculate_extra_fuel(masses):
    total = []
    for mass in masses:
        fuel_with_extra = 0
        fuel = fuel_calc(mass)
        fuel_with_extra += fuel
        while True:
            fuel = fuel_calc(fuel)
            if fuel <= 0:
                break
            fuel_with_extra += fuel
        total.append(fuel_with_extra)
    return total


@pytest.fixture
def example_masses():
    return [12, 14, 1969, 100756]


def test_part_one_testcase(example_masses):
    assert calculate_fuels(example_masses) == [2, 2, 654, 33583]


def test_part_two_testcase(example_masses):
    assert calculate_extra_fuel(example_masses) == [2, 2, 966, 50346]


if __name__ == '__main__':
    with open('day1.in', 'r') as f:
        print(sum(calculate_fuels(map(int, f.readlines()))))
    with open('day1.in', 'r') as f:
        print(sum(calculate_extra_fuel(map(int, f.readlines()))))
