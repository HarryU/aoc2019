use aoc2019::read_ints_from_file;
use itertools::iterate;

fn calculate_fuel(mass: i32) -> i32 {
    (mass / 3) - 2
}

fn calculate_fuel_for_fuel(mass: i32) -> i32 {
    iterate(mass, |&i| calculate_fuel(i))
        .take_while(|&x| x.is_positive())
        .skip(1)
        .sum()
}

fn calculate_fuel_for_masses(mass: &Vec<i32>) -> i32 {
    mass.iter().map(|x| calculate_fuel(*x)).sum()
}

fn calculate_fuel_for_masses_and_fuel(mass: &Vec<i32>) -> i32 {
    mass.iter().map(|x| calculate_fuel_for_fuel(*x)).sum()
}

fn main() {
    let input = read_ints_from_file("day1.in");
    println!("Part one: {}", calculate_fuel_for_masses(&input));
    println!("Part two: {}", calculate_fuel_for_masses_and_fuel(&input));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn calculate_fuel_for_12_gives_2() {
        assert_eq!(calculate_fuel(12), 2);
    }

    #[test]
    fn calculate_fuel_for_14_gives_2() {
        assert_eq!(calculate_fuel(14), 2);
    }

    #[test]
    fn calculate_total_fuel_for_12_and_14_gives_4() {
        assert_eq!(calculate_fuel_for_masses_and_fuel(&vec![12, 14]), 4);
    }

    #[test]
    fn calculate_fuel_including_fuel_for_1969() {
        assert_eq!(calculate_fuel_for_masses_and_fuel(&vec![1969]), 966);
    }
}
